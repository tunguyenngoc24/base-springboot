Hi,<br/>

Thanks for using Balance Meditation! Please confirm your email address by clicking on the link below.<br/>

${VERIFICATION_URL}<br/>

If you did not sign up for a Balance Meditation account please disregard this email.<br/>

The Gobit team.