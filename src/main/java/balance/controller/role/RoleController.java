package balance.controller.role;

import balance.model.role.Role;
import balance.model.role.UserRole;
import balance.service.role.IRoleService;
import balance.service.role.IUserRoleService;
import balance.util.api.BaseResponseMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(path = "/api/v1/private/role")
public class RoleController {
    @Autowired
    private IRoleService roleService;
    private HashMap<String, Object> results;

    // add create role api
    @PostMapping(path = "/create")
    public @ResponseBody
    HttpEntity<HashMap> Create (@RequestBody Role role, HttpServletResponse response, HttpServletRequest request) {
        results = new HashMap<String, Object>();
        role.setCtime(new Timestamp(System.currentTimeMillis()));
        role.setMtime(new Timestamp(System.currentTimeMillis()));
        Role ro = roleService.save(role);
        if (ro == null) {
            return BaseResponseMethod.SendFailResponse(request, response, results, "error_create_role");
        } else {
            return BaseResponseMethod.SendSuccessResponse(request, response, results, ro);
        }
    }

    //get all role by userId api
    @GetMapping(path = "/get_all_by_user")
    public @ResponseBody
    HttpEntity<HashMap> GetAllByUser(HttpServletResponse response, HttpServletRequest request, @RequestParam Integer userId) {
        results = new HashMap<String, Object>();
        List<Role> roleList;
        if (userId == null) {
            roleList = roleService.findAll();
        } else {
            roleList = roleService.findAllByUserId(userId);
        }
        if(roleList == null) {
            return BaseResponseMethod.SendSuccessResponse(request, response, results, null);
        } else {
            return BaseResponseMethod.SendSuccessResponse(request, response, results, roleList);
        }
    }

    // delete role api
    @DeleteMapping(path = "/delete/{id}")
    public @ResponseBody
    HttpEntity<HashMap> DeleteRole(@PathVariable Integer id, HttpServletRequest request, HttpServletResponse response) {
        results = new HashMap<String, Object>();
        if (!roleService.existsById(id)) {
            return BaseResponseMethod.SendFailResponse(request, response, results, "user_not_found");
        } else {
            roleService.deleteById(id);
            return BaseResponseMethod.SendSuccessResponse(request, response, results, null);
        }
    }

    // update by id api
    @PutMapping(path = "/update/{id}")
    public @ResponseBody
    HttpEntity<HashMap> UpdateById(@PathVariable Integer id, @RequestBody Role role, HttpServletResponse response, HttpServletRequest request) {
        results = new HashMap<String, Object>();
        if (!roleService.existsById(id)) {
            return BaseResponseMethod.SendFailResponse(request, response, results, "role_not_found");
        } else {
            Role ro = roleService.getOne(id);
            ro.setName(role.getName());
            ro.setMtime(new Timestamp(System.currentTimeMillis()));
            return BaseResponseMethod.SendSuccessResponse(request, response, results, roleService.save(ro));
        }
    }
}

