//package balance.controller.role;
//
//import balance.model.role.Role;
//import balance.model.role.UserRole;
//import balance.service.role.IRoleService;
//import balance.service.role.IUserRoleService;
//import balance.util.api.BaseResponseMethod;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpEntity;
//import org.springframework.web.bind.annotation.*;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.sql.Timestamp;
//import java.util.HashMap;
//import java.util.List;
//
//@CrossOrigin
//@RestController
//@RequestMapping(path = "/api/v1/private/user_role")
//public class UserRoleController {
//    @Autowired
//    private IUserRoleService userRoleService;
//    private HashMap<String, Object> results;
//
//    // add role to user api
//    @PostMapping(path = "/create")
//    public @ResponseBody
//    HttpEntity<HashMap> Create (@RequestBody UserRole userRole, HttpServletResponse response, HttpServletRequest request) {
//        results = new HashMap<String, Object>();
//        userRole.setCtime(new Timestamp(System.currentTimeMillis()));
//        userRole.setMtime(new Timestamp(System.currentTimeMillis()));
//        UserRole ro = userRoleService.save(userRole);
//        if (ro == null) {
//            return BaseResponseMethod.SendFailResponse(request, response, results, "error_add_role");
//        } else {
//            return BaseResponseMethod.SendSuccessResponse(request, response, results, ro);
//        }
//    }
//
//    // remove role from user api
//    @DeleteMapping(path = "/delete/{id}")
//    public @ResponseBody
//    HttpEntity<HashMap> DeleteUser(@PathVariable Integer id, HttpServletRequest request, HttpServletResponse response) {
//        results = new HashMap<String, Object>();
//        if (!userRoleService.existsById(id)) {
//            return BaseResponseMethod.SendFailResponse(request, response, results, "user_not_found");
//        } else {
//            roleService.deleteById(id);
//            return BaseResponseMethod.SendSuccessResponse(request, response, results, null);
//        }
//    }
//}
//
