package balance.controller.auth;

import balance.model.user.User;
import balance.security.jwt.AccountCredentials;
import balance.security.jwt.TokenAuthenticationService;
import balance.service.user.IUserService;
import balance.util.api.BaseResponseMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping(path = "/api/v1/auth")
public class AuthController {
    @Autowired
    private IUserService userService;
    @Autowired
    TokenAuthenticationService tokenAuthenticationService;
    private HashMap<String, Object> results;

    // login api
    @PostMapping(path = "/login")
    public @ResponseBody
    HttpEntity<HashMap> Login (@RequestBody AccountCredentials auth, HttpServletResponse response, HttpServletRequest request) {
        results = new HashMap<String, Object>();
        Map<Object, Object> model = new HashMap<>();
        User user = userService.findByEmail(auth.getUsername());
        if(user == null) {
            return BaseResponseMethod.SendFailResponse(request, response, results, "user_not_found");
        } else {
            //if (user.getEmailConfirm()) {
            String token = tokenAuthenticationService.addAuthentication(user.getEmail());
            model.put("email", user.getEmail());
            model.put("token", token);
            BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
            if (bCryptPasswordEncoder.matches(auth.getPassword(), user.getPassword())) {
                return BaseResponseMethod.SendSuccessResponse(request, response, results, model);
            } else {
                return BaseResponseMethod.SendFailResponse(request, response, results, "password_not_compare");
            }
            //} else {
                //return BaseResponseMethod.SendFailResponse(request, response, results, "email_not_confirm");
            //}
        }
    }

    // logout api
    @GetMapping(path = "/logout")
    public @ResponseBody
    ResponseEntity<HttpStatus> Logout(HttpServletResponse response, HttpServletRequest request) {
        return BaseResponseMethod.SendStatusResponse(request, response, HttpStatus.OK);
    }
}
