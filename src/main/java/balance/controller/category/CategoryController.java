package balance.controller.category;

import balance.model.category.Category;
import balance.model.role.Role;
import balance.service.category.ICategoryService;
import balance.service.role.IRoleService;
import balance.util.api.BaseResponseMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(path = "/api/v1/private/category")
public class CategoryController {
    @Autowired
    private ICategoryService categoryService;
    private HashMap<String, Object> results;

    // add create category api
    @PostMapping(path = "/create")
    public @ResponseBody
    HttpEntity<HashMap> Create (@RequestBody Category category, HttpServletResponse response, HttpServletRequest request) {
        results = new HashMap<String, Object>();
        category.setCtime(new Timestamp(System.currentTimeMillis()));
        category.setMtime(new Timestamp(System.currentTimeMillis()));
        Category cate = categoryService.save(category);
        if (cate == null) {
            return BaseResponseMethod.SendFailResponse(request, response, results, "error_create_category");
        } else {
            return BaseResponseMethod.SendSuccessResponse(request, response, results, cate);
        }
    }

    //get all category api
    @GetMapping(path = "/get_all")
    public @ResponseBody
    HttpEntity<HashMap> GetAll(HttpServletResponse response, HttpServletRequest request) {
        results = new HashMap<String, Object>();
        List<Category> categoryList;
        categoryList = categoryService.findAll();
        if(categoryList == null) {
            return BaseResponseMethod.SendSuccessResponse(request, response, results, null);
        } else {
            return BaseResponseMethod.SendSuccessResponse(request, response, results, categoryList);
        }
    }

    // delete category api
    @DeleteMapping(path = "/delete/{id}")
    public @ResponseBody
    HttpEntity<HashMap> Delete(@PathVariable Integer id, HttpServletRequest request, HttpServletResponse response) {
        results = new HashMap<String, Object>();
        if (!categoryService.existsById(id)) {
            return BaseResponseMethod.SendFailResponse(request, response, results, "category_not_found");
        } else {
            categoryService.deleteById(id);
            return BaseResponseMethod.SendSuccessResponse(request, response, results, null);
        }
    }
//
//    // update category by id api
//    @PutMapping(path = "/update/{id}")
//    public @ResponseBody
//    HttpEntity<HashMap> UpdateById(@PathVariable Integer id, @RequestBody Role role, HttpServletResponse response, HttpServletRequest request) {
//        results = new HashMap<String, Object>();
//        if (!categoryService.existsById(id)) {
//            return BaseResponseMethod.SendFailResponse(request, response, results, "role_not_found");
//        } else {
//            Category cate = categoryService.findById(id);
//            ro.setName(role.getName());
//            ro.setMtime(new Timestamp(System.currentTimeMillis()));
//            return BaseResponseMethod.SendSuccessResponse(request, response, results, categoryService.save(cate));
//        }
//    }
}

