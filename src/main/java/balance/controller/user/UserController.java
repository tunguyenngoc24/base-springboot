package balance.controller.user;

import balance.model.auth.ChangePassword;
import balance.model.user.User;
import balance.service.user.IUserService;
import balance.util.api.BaseResponseMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Timestamp;
import java.util.*;

@CrossOrigin
@RestController
@RequestMapping(path = "/api/v1")
public class UserController {
    @Autowired
    private IUserService userService;
    private HashMap<String, Object> results;

    // add create user api
    @PostMapping(path = "/public/user/register")
    public @ResponseBody
    HttpEntity<HashMap> Register (@RequestBody User user, HttpServletResponse response, HttpServletRequest request) {
        results = new HashMap<String, Object>();
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        String email = user.getEmail();
        User user1 = userService.findByEmail(email);
        if (user1 != null) {
            return BaseResponseMethod.SendFailResponse(request, response, results, "email_exists");
        } else {
            user.setCtime(new Timestamp(System.currentTimeMillis()));
            user.setMtime(new Timestamp(System.currentTimeMillis()));
            user.setEmailConfirm(false);
            user.setLevelInAcademy("1-1");
            user.setLevelInCenter("1-1");
            user.setIsMember(false);
            User us = userService.save(user);
            if (us == null) {
                return BaseResponseMethod.SendFailResponse(request, response, results, "error_register_user");
            } else {
                return BaseResponseMethod.SendSuccessResponse(request, response, results, us);
            }
        }
    }

    // get all user api
    @GetMapping(path = "/private/user/get_all")
    public @ResponseBody
    HttpEntity<HashMap> GetAllUser(HttpServletResponse response, HttpServletRequest request, @RequestParam Integer pageNo, @RequestParam Integer pageSize, @RequestParam Boolean isMember) {
        results = new HashMap<>();
        List<User> userList;
        if (pageNo == null || pageSize == null) {
            userList = userService.findAll();
        } else {
            userList = userService.findAllPaging(pageNo, pageSize);
        }
        ArrayList<User> userList1 = new ArrayList<>();
        if (isMember == null) {
            userList1 = (ArrayList<User>) userList;
        } else {
            if (isMember) {
                for (User user : userList) {
                    if (user.getIsMember()) {
                        userList1.add(user);
                    }
                }
            } else {
                for (User user : userList) {
                    if (!user.getIsMember()) {
                        userList1.add(user);
                    }
                }
            }
        }
        if(userList1 == null) {
            return BaseResponseMethod.SendSuccessResponse(request, response, results, null);
        } else {
            return BaseResponseMethod.SendSuccessResponse(request, response, results, userList1);
        }
    }

    // get user by id api
    @GetMapping(path = "/private/user/get_by_id/{id}")
    public @ResponseBody
    HttpEntity<HashMap> GetById(@PathVariable Integer id, HttpServletResponse response, HttpServletRequest request) {
        results = new HashMap<String, Object>();
        if (!userService.existsById(id)) {
            return BaseResponseMethod.SendFailResponse(request, response, results, "user_not_found");
        } else {
            Optional<User> user = userService.findById(id);
            return BaseResponseMethod.SendSuccessResponse(request, response, results, user);
        }
    }

    // delete user api
    @DeleteMapping(path = "/private/user/delete/{id}")
    public @ResponseBody
    HttpEntity<HashMap> DeleteUser(@PathVariable Integer id, HttpServletRequest request, HttpServletResponse response) {
        results = new HashMap<String, Object>();
        if (!userService.existsById(id)) {
            return BaseResponseMethod.SendFailResponse(request, response, results, "user_not_found");
        } else {
            userService.deleteById(id);
            return BaseResponseMethod.SendSuccessResponse(request, response, results, null);
        }
    }

    // change password api
    @PutMapping(path = "/private/user/change_password")
    public @ResponseBody
    HttpEntity<HashMap> ChangePassword(@RequestBody ChangePassword changePassword, HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findByEmail(auth.getName());
        if (user == null) {
            return BaseResponseMethod.SendFailResponse(request, response, results, "user_not_found");
        } else {
            BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
            String newPassword = changePassword.getNewPassword();
            String rePassword = changePassword.getRePassword();
            if (bCryptPasswordEncoder.matches(changePassword.getOldPassword(), user.getPassword()) && newPassword.equals(rePassword)) {
                user.setPassword(bCryptPasswordEncoder.encode(changePassword.getNewPassword()));
                user.setMtime(new Timestamp(System.currentTimeMillis()));
                userService.save(user);
                return BaseResponseMethod.SendSuccessResponse(request, response, results, null);
            } else {
                return BaseResponseMethod.SendFailResponse(request, response, results, "password_not_matches");
            }
        }
    }

    // get profile api
    @GetMapping(path = "/private/user/get_profile")
    public @ResponseBody
    HttpEntity<HashMap> GetProfile(HttpServletResponse response, HttpServletRequest request) {
        results = new HashMap<String, Object>();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findByEmail(auth.getName());
        if (user == null) {
            return BaseResponseMethod.SendFailResponse(request, response, results, "profile_not_found");
        } else {
            return BaseResponseMethod.SendSuccessResponse(request, response, results, user);
        }
    }

    // update by id api
    @PutMapping(path = "/private/user/update/{id}")
    public @ResponseBody
    HttpEntity<HashMap> UpdateById(@PathVariable Integer id, @RequestBody User user, HttpServletResponse response, HttpServletRequest request) {
        results = new HashMap<String, Object>();
        if (!userService.existsById(id)) {
            return BaseResponseMethod.SendFailResponse(request, response, results, "user_not_found");
        } else {
            User us = userService.getOne(id);
            us.setAddress(user.getAddress());
            us.setBankAccount(user.getBankAccount());
            us.setDob(user.getDob());
            us.setAvatar(user.getAvatar());
            us.setIsMember(user.getIsMember());
            us.setLevelInAcademy(user.getLevelInAcademy());
            us.setLevelInCenter(user.getLevelInCenter());
            us.setName(user.getName());
            us.setNickName(user.getNickName());
            us.setOnline(user.getOnline());
            us.setPhone(user.getPhone());
            us.setStatus(user.getStatus());
            us.setMtime(new Timestamp(System.currentTimeMillis()));
            return BaseResponseMethod.SendSuccessResponse(request, response, results, userService.save(us));
        }
    }
}

