//package balance.controller.Post;
//
//import balance.model.Post.PostModel;
//import balance.service.Post.PostService;
//import balance.util.api.BaseResponseMethod;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpEntity;
//import org.springframework.http.MediaType;
//import org.springframework.web.bind.annotation.*;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.validation.Valid;
//import java.sql.Timestamp;
//import java.util.HashMap;
//import java.util.List;
//
//@CrossOrigin
//@RestController
//@RequestMapping(path = "/api/v1")
//public class PostController {
//    public static Logger logger = LoggerFactory.getLogger(PostController.class);
//    private HashMap<String, Object> results;
//
//    @Autowired
//    PostService postService;
//
//    // list all
//    @RequestMapping(value = "/public/post/get_all", method = RequestMethod.GET)
//    public @ResponseBody
//    HttpEntity<HashMap> getAllLang(HttpServletResponse response, HttpServletRequest request) {
//        List<PostModel> allPost = postService.findAll();
//        results = new HashMap<String, Object>();
//        if (allPost.isEmpty()) {
//            return BaseResponseMethod.SendSuccessResponse(request, response, results, null);
//        }
//        return BaseResponseMethod.SendSuccessResponse(request, response, results, allPost);
//    }
//
//    // create
//    @RequestMapping(value = "/private/post/create", method = RequestMethod.POST)
//    public @ResponseBody
//    HttpEntity<HashMap> NewLang (@Valid PostModel postForm, HttpServletRequest request, HttpServletResponse response) {
//        results = new HashMap<String, Object>();
//        postForm.setMtime(new Timestamp(System.currentTimeMillis()));
//        PostModel createdPost = postService.save(postForm);
//        return BaseResponseMethod.SendSuccessResponse(request, response, results, createdPost);
//    }
//
//    // get ID
//    @RequestMapping(value = "/public/post/get_by_id/{id}", method = RequestMethod.GET)
//    public HttpEntity<HashMap> getByID(@PathVariable(value = "id") Integer id, HttpServletRequest request, HttpServletResponse response) {
//        PostModel post = postService.findById(id);
//        results = new HashMap<String, Object>();
//        if(postService.existsById(id)) {
//            return BaseResponseMethod.SendSuccessResponse(request, response, results, post);
//        }
//        return BaseResponseMethod.SendFailResponse(request, response, results, "post_not_found");
//    }
//
//
//    // update
//    @RequestMapping(value = "/private/post/update/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
//    public @ResponseBody HttpEntity<HashMap> updateLang(@PathVariable("id") Integer id, @Valid PostModel postForm, HttpServletRequest request, HttpServletResponse response) {
//        results = new HashMap<String, Object>();
//        if(postService.existsById(id)) {
//            PostModel post = postService.findById(id);
//            post.setParentId(postForm.getParentId());
//            post.setDisplayOrder(postForm.getDisplayOrder());
//            post.setPostTime(postForm.getPostTime());
//            post.setStatus(postForm.getStatus());
//            post.setType(postForm.getType());
//            post.setMtime(new Timestamp(System.currentTimeMillis()));
//            PostModel updatedPost = postService.save(post);
//            return BaseResponseMethod.SendSuccessResponse(request, response, results, updatedPost);
//        }
//        return BaseResponseMethod.SendFailResponse(request,response, results, "post_not_found");
//    }
//
//    //delete
//    @RequestMapping(value = "/private/lang/post/{id}", method = RequestMethod.DELETE)
//    public HttpEntity<HashMap> deleteLanguages(@PathVariable(value = "id") Integer id, HttpServletResponse response, HttpServletRequest request) {
//        PostModel post = postService.findById(id);
//        results = new HashMap<String, Object>();
//        if(postService.existsById(id)) {
//            postService.deleteById(id);
//            return BaseResponseMethod.SendSuccessResponse(request, response, results, "Success");
//        }
//        return BaseResponseMethod.SendFailResponse(request, response, results, "post_not_found");
//
//    }
//}
