package balance.controller.Languages;


import balance.model.Language.LanguageModel;
import balance.service.languages.LanguageService;

import balance.util.api.BaseResponseMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.*;

@CrossOrigin
@RestController
@RequestMapping(path = "/api/v1")
public class LanguageController {
    public static Logger logger = LoggerFactory.getLogger(LanguageController.class);
    private HashMap<String, Object> results;

    @Autowired
    LanguageService LangService;

    // list all
    @RequestMapping(value = "/public/lang/get_all", method = RequestMethod.GET)
    public @ResponseBody HttpEntity<HashMap> getAllLang(HttpServletResponse response, HttpServletRequest request) {
        List<LanguageModel> allLang = LangService.findAll();
        results = new HashMap<String, Object>();
        if (allLang.isEmpty()) {
            return BaseResponseMethod.SendFailResponse(request, response, results, "language_not_found");
        }
        return BaseResponseMethod.SendSuccessResponse(request, response, results, allLang);
    }

    // create
    @RequestMapping(value = "/private/lang/create", method = RequestMethod.POST)
    public @ResponseBody
    HttpEntity<HashMap> NewLang (@RequestParam String code,@Valid LanguageModel langForm, HttpServletRequest request, HttpServletResponse response) {
        LanguageModel lang = LangService.findByCode(code);
        results = new HashMap<String, Object>();
        if(lang == null) {
            langForm.setCtime(new Timestamp(System.currentTimeMillis()));
            LanguageModel createdLanguage = LangService.save(langForm);
            return BaseResponseMethod.SendSuccessResponse(request, response, results, createdLanguage);
        }
        return BaseResponseMethod.SendFailResponse(request, response, results, "language_is_exits");
    }

    // get ID
    @RequestMapping(value = "/private/lang/get_by_id/{id}", method = RequestMethod.GET)
    public HttpEntity<HashMap> getByID(@PathVariable(value = "id") Integer id, HttpServletRequest request, HttpServletResponse response) {
        LanguageModel lang = LangService.findById(id);
        results = new HashMap<String, Object>();
        if(LangService.existsById(id)) {
            return BaseResponseMethod.SendSuccessResponse(request, response, results, lang);
        }
        return BaseResponseMethod.SendFailResponse(request, response, results, "languague_not_found");
    }

    // get by code
    @RequestMapping(value = "/private/lang/get_by_code/{code}", method = RequestMethod.GET)
    public HttpEntity<HashMap> getByID(@PathVariable String code, HttpServletRequest request, HttpServletResponse response) {
        LanguageModel lang = LangService.findByCode(code);
        results = new HashMap<String, Object>();
        if(lang != null) {
            return BaseResponseMethod.SendSuccessResponse(request, response, results, lang);
        }
        return BaseResponseMethod.SendFailResponse(request, response, results, "languague_not_found");
    }

    // update
    @RequestMapping(value = "/private/lang/update/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public @ResponseBody HttpEntity<HashMap> updateLang(@PathVariable("id") Integer id, @Valid LanguageModel langForm, HttpServletRequest request, HttpServletResponse response) {
        results = new HashMap<String, Object>();

        if(LangService.existsById(id)) {
            LanguageModel lang = LangService.findById(id);
            LanguageModel check = LangService.findByCode(langForm.getCode());
            if(check != null && check.getId() == id) {
                lang.setName(langForm.getName());
                lang.setCode(langForm.getCode());
                lang.setMtime(new Timestamp(System.currentTimeMillis()));
                LanguageModel updatedLanguagues = LangService.save(lang);
                System.out.println(langForm.getCode());
                return BaseResponseMethod.SendSuccessResponse(request, response, results, updatedLanguagues);
            }
            return BaseResponseMethod.SendFailResponse(request,response, results, "languague_sub_name_is_exits");

        }
        return BaseResponseMethod.SendFailResponse(request,response, results, "languague_not_found");
    }

    //delete
    @RequestMapping(value = "/private/lang/delete/{id}", method = RequestMethod.DELETE)
    public HttpEntity<HashMap> deleteLanguages(@PathVariable(value = "id") Integer id, HttpServletResponse response, HttpServletRequest request) {
        LanguageModel lang = LangService.findById(id);
        results = new HashMap<String, Object>();
        if(LangService.existsById(id)) {
            LangService.deleteById(id);
            return BaseResponseMethod.SendSuccessResponse(request, response, results, "Success");
        }
        return BaseResponseMethod.SendFailResponse(request, response, results, "menu_not_found");

    }
}
