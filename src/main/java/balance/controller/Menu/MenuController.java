package balance.controller.Menu;

import balance.model.Language.LanguageModel;
import balance.model.Menu.MenuInfo;
import balance.model.Menu.Menus;
import balance.model.Menu.MenuTranslators;
import balance.repository.MenuTrans.IMenuTransRepository;
import balance.service.Menu.MenuService;
import balance.util.api.BaseResponseMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping(path = "/api/v1")
public class MenuController {
    private HashMap<String, Object> results;

    @Autowired
    MenuService menuService;

    @Autowired
    IMenuTransRepository iMenuTransRepository;

    // create
    @RequestMapping(value = "/private/menu/create", method = RequestMethod.POST)
    public @ResponseBody
    HttpEntity<HashMap> NewMenu (@Valid Menus menusForm, @Valid MenuTranslators menuTrans, HttpServletRequest request, HttpServletResponse response) {
        results = new HashMap<String, Object>();
        menusForm.setCtime(new Timestamp(System.currentTimeMillis()));
        menusForm.setMenuTranslators(menuTrans);
        menuTrans.setMenus(menusForm);
        Menus createdMenus = menuService.save(menusForm);

        return BaseResponseMethod.SendSuccessResponse(request, response, results, createdMenus);
    }

    //get all menu by language
    @RequestMapping(value = "/public/menu/get_all/{code}", method = RequestMethod.GET)
    public @ResponseBody
    HttpEntity<HashMap> getAllMenuByLang(@PathVariable(value = "code") String code, HttpServletResponse response, HttpServletRequest request) {
        List<MenuInfo> transList = iMenuTransRepository.findByLang(code);
        results = new HashMap<String, Object>();
        if(transList.isEmpty()) {
            return BaseResponseMethod.SendSuccessResponse(request, response, results, null);
        }
        return BaseResponseMethod.SendSuccessResponse(request, response, results, transList);
    }

    // get by id
    @RequestMapping(value = "/private/menu/get_by_id/{id}", method = RequestMethod.GET)
    public HttpEntity<HashMap> findMenuByID(@PathVariable(value = "id") Integer id, HttpServletRequest request, HttpServletResponse response) {
        List<MenuInfo> menuInfo = iMenuTransRepository.findByID(id);
        results = new HashMap<String, Object>();
        if(menuInfo.isEmpty()) {
            return BaseResponseMethod.SendMessageResponse(request,response, results, "Menu not found");
        }
        return BaseResponseMethod.SendSuccessResponse(request,response,results,menuInfo);
    }

    // get menu info by language
    @RequestMapping(value = "/private/menu/get_menu_by_lang/", method = RequestMethod.POST)
    public @ResponseBody HttpEntity<HashMap> getMenuInfoByLang(@RequestBody MenuInfo menu, HttpServletRequest request, HttpServletResponse response) {
        Integer menuId = menu.getMenuId();
        String code = menu.getCode();
        MenuInfo menuInfo = iMenuTransRepository.getMenuInfoByLang(menuId,code);
        results = new HashMap<String, Object>();
        if(menuInfo != null) {
            return BaseResponseMethod.SendSuccessResponse(request,response,results,menuInfo);
        }
        return BaseResponseMethod.SendMessageResponse(request,response, results, "Menu not found");

    }

    // update menu by lang
    @RequestMapping(value = "/private/menu/update/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public @ResponseBody HttpEntity<HashMap> updateMenu(@PathVariable("id") Integer id, @Valid MenuInfo menuInfo, HttpServletRequest request, HttpServletResponse response) {
        results = new HashMap<String, Object>();
        if(iMenuTransRepository.existsById(id)) {
            MenuInfo menu = iMenuTransRepository.getOne(id);
            menu.setName(menuInfo.getName());
            menu.setLink(menuInfo.getLink());
            iMenuTransRepository.save(menu);
            return BaseResponseMethod.SendMessageResponse(request, response, results, "Update success");
        }
        return BaseResponseMethod.SendFailResponse(request,response, results, "menu_not_found");
    }

    // delete menu
    @RequestMapping(value = "/private/menu/delete/{id}", method = RequestMethod.DELETE)
    public HttpEntity<HashMap> deleteMenu(@PathVariable(value = "id") Integer id, HttpServletResponse response, HttpServletRequest request) {
        results = new HashMap<String, Object>();
        if(menuService.existsById(id)) {
            menuService.deleteById(id);
            return BaseResponseMethod.SendSuccessResponse(request, response, results, "Success");
        }
        return BaseResponseMethod.SendFailResponse(request, response, results, "languague_not_found");
    }

    // delete menu language
    @RequestMapping(value = "/private/menu/delete_lang/{id}", method = RequestMethod.DELETE)
    public HttpEntity<HashMap> deleteMenuLang(@PathVariable(value = "id") Integer id, HttpServletResponse response, HttpServletRequest request) {
        results = new HashMap<String, Object>();
        if(iMenuTransRepository.existsById(id)) {
            iMenuTransRepository.deleteById(id);
            return BaseResponseMethod.SendSuccessResponse(request, response, results, "Success");
        }
        return BaseResponseMethod.SendFailResponse(request, response, results, "languague_not_found");
    }
}
