//package balance.security.jwt;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.AuthenticationException;
//import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
//import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
//
//import javax.servlet.FilterChain;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//
//public class JWTLoginFilter extends AbstractAuthenticationProcessingFilter {
//    private TokenAuthenticationService tokenAuthenticationService;
//
//    public JWTLoginFilter(String url, AuthenticationManager authenticationManager) {
//        super(new AntPathRequestMatcher(url));
//        setAuthenticationManager(authenticationManager);
//        tokenAuthenticationService = new TokenAuthenticationService();
//    }
//
//    @Override
//    public Authentication attemptAuthentication(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
//            throws AuthenticationException, IOException, ServletException {
//        AccountCredentials credentials = new ObjectMapper().readValue(httpServletRequest.getInputStream(), AccountCredentials.class);
//        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(credentials.getUsername(), credentials.getPassword());
//        return getAuthenticationManager().authenticate(token);
//    }
//
//    @Override
//    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authentication)
//            throws IOException, ServletException {
//        //String name = authentication.getName();
//        //tokenAuthenticationService.addAuthentication(response, name);
//
//        // Authorize (allow) all domains to consume the content
//        response.setHeader("Access-Control-Allow-Origin", "*");
//        response.setHeader("Access-Control-Allow-Methods","GET, POST, OPTIONS, PUT, DELETE");
//        response.setHeader("Access-Control-Allow-Headers","*");
//        response.setHeader("Access-Control-Max-Age","3600");
//        response.setHeader("Access-Control-Allow-Credentials","true");
//        //servletResponse.setContentType("application/json");
//
//        // For HTTP OPTIONS
//        if (request.getMethod().equals("OPTIONS")) {
//            response.setStatus(HttpServletResponse.SC_ACCEPTED);
//            return;
//        }
//    }
//}