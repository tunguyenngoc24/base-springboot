package balance.repository.Languages;

import balance.model.Language.LanguageModel;
import org.springframework.data.jpa.repository.Query;

public interface ILangRepositoryCustom {
    LanguageModel findByCode(String Code);
}
