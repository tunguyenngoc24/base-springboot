package balance.repository.Languages;

import balance.model.Language.LanguageModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ILangRepository extends JpaRepository<LanguageModel, Integer>, ILangRepositoryCustom {
        }
