package balance.repository.Languages;

import balance.model.Language.LanguageModel;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Repository
@Transactional(readOnly = true)
public class LangRepositoryImpl implements ILangRepositoryCustom {
    @PersistenceContext
    EntityManager entityManager;

    @Override
    public LanguageModel findByCode(String code) {
        Query query = entityManager.createNativeQuery("SELECT * from languages where code LIKE ?", LanguageModel.class);
        query.setParameter(1, code + "%");

        return (LanguageModel) query.getSingleResult();
    }
}
