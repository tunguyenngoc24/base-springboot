package balance.repository.Post;

import balance.model.Post.PostModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostRepository extends JpaRepository<PostModel, Integer> {
}
