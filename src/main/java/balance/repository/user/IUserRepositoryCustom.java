package balance.repository.user;

import balance.model.user.User;

public interface IUserRepositoryCustom {
    User findByEmail(String email);
}
