package balance.repository.user;

import balance.model.user.User;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Repository
@Transactional(readOnly = true)
public class UserRepositoryImpl implements IUserRepositoryCustom {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public User findByEmail(String email) {
        Query query = entityManager.createNativeQuery("SELECT * from users where email LIKE ?", User.class);
        query.setParameter(1, email + "%");

        return (User) query.getSingleResult();
    }
}
