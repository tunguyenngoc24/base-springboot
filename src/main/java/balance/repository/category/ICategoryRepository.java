package balance.repository.category;

import balance.model.category.Category;
import balance.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ICategoryRepository extends JpaRepository<Category, Integer>, ICategoryRepositoryCustom {

}
