package balance.repository.category;

import balance.model.user.User;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Repository
@Transactional(readOnly = true)
public class CategoryRepositoryImpl implements ICategoryRepositoryCustom {

    @PersistenceContext
    EntityManager entityManager;
}
