package balance.repository.MenuTrans;

import balance.model.Language.LanguageModel;
import balance.model.Menu.MenuInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IMenuTransRepository extends JpaRepository<MenuInfo, Integer> {
    @Query(value = "SELECT * FROM menu_translators e WHERE e.language_code = ?1", nativeQuery = true)
    List<MenuInfo> findByLang(String code);

    // get by menu id
    @Query(value = "SELECT * FROM menu_translators e WHERE e.menu_id = ?1", nativeQuery = true)
    List<MenuInfo> findByID(Integer menu_id);

    // get by menu id and language
    @Query(value = "SELECT * FROM menu_translators e WHERE e.menu_id = :menu_id AND e.language_code = :code", nativeQuery = true)
    MenuInfo getMenuInfoByLang(@Param("menu_id") Integer menu_id, @Param("code") String code);

    // delete all menu language
    @Query(value = "DELETE FROM menu_translators e WHERE e.menu_id = ?1", nativeQuery = true)
    MenuInfo deleteAllById(Integer id);
}
