package balance.repository.Menu;

import balance.model.Menu.MenuInfo;
import balance.model.Menu.MenuTranslators;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IMenuTransRepository extends JpaRepository<MenuTranslators, Integer> {

    // delete all menu language
    @Query(value = "DELETE FROM menu_translators e WHERE e.menu_id = ?1", nativeQuery = true)
    MenuTranslators deleteAllById(Integer id);

    @Query(value = "SELECT * FROM menu_translators e WHERE e.language_code = ?1", nativeQuery = true)
    List<MenuInfo> findByLang(String code);

    // get by menu id
    @Query(value = "SELECT * FROM menu_translators e WHERE e.menu_id = ?1", nativeQuery = true)
    List<MenuInfo> findByID(Integer menu_id);

    // get by menu id and language
    @Query(value = "SELECT * FROM menu_translators e WHERE e.menu_id = :menu_id AND e.language_code = :code", nativeQuery = true)
    MenuInfo getMenuInfoByLang(@Param("menu_id") Integer menu_id, @Param("code") String code);
    MenuTranslators save(MenuTranslators menuInfo);
}
