package balance.repository.Menu;

import balance.model.Menu.Menus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IMenuRepository extends JpaRepository<Menus, Integer> {

}
