package balance.repository.role;

import balance.model.role.UserRole;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
@Transactional(readOnly = true)
public class UserRoleRepositoryImpl implements IUserRoleRepositoryCustom {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public List<UserRole> findAllByUserId(Integer userId) {
        Query query = entityManager.createNativeQuery("select ur.* from user_roles ur join roles r on ur.role_id = r.id" +
                "join user u on ur.user_id = u.id where u.id = ?", UserRole.class);
        query.setParameter(1, userId);
        return query.getResultList();
    }
}
