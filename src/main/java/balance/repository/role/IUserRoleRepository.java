package balance.repository.role;

import balance.model.role.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IUserRoleRepository extends JpaRepository<UserRole, Integer>, IUserRoleRepositoryCustom {

}
