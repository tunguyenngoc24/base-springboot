package balance.repository.role;

import balance.model.role.UserRole;

import java.util.List;

public interface IUserRoleRepositoryCustom {
    List<UserRole> findAllByUserId(Integer userId);
}
