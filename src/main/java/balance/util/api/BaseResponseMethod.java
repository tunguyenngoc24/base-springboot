package balance.util.api;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

public class BaseResponseMethod {
    public static HttpEntity<HashMap> SendSuccessResponse(HttpServletRequest request, HttpServletResponse response, HashMap<String, Object> results, Object object) {
        results.put("success","true");
        results.put("data",object);
        return new HttpEntity(results);
    }

    public static HttpEntity<HashMap> SendFailResponse(HttpServletRequest request, HttpServletResponse response, HashMap<String, Object> results, String message) {
        results.put("message",message);
        results.put("success","false");
        return new HttpEntity(results);
    }

    public static HttpEntity<HashMap> SendMessageResponse(HttpServletRequest request, HttpServletResponse response, HashMap<String, Object> results, String message) {
        results.put("message",message);
        results.put("success","true");
        return new HttpEntity(results);
    }

    public static ResponseEntity<HttpStatus> SendStatusResponse(HttpServletRequest request, HttpServletResponse response, HttpStatus status) {
        return ResponseEntity.ok(status);
    }
}
