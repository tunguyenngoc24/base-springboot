package balance.util.email;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

public class SendMailService {
    private static JavaMailSender javaMailSender;

    public static boolean sendMail(String toEmail, String subject, String body) {
        try
        {
            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setTo(toEmail);
            mailMessage.setSubject(subject);
            mailMessage.setText(body);
            javaMailSender.send(mailMessage);
            return true;
        } catch (Exception ex) { }

        return false;
    }
}
