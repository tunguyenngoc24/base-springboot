package balance.service.languages;

import balance.model.Language.LanguageModel;
import balance.repository.Languages.ILangRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
@RequiredArgsConstructor
public class LanguageService implements ILanguagesService {
    @Autowired
    private ILangRepository langRepository;

    @Override
    public List<LanguageModel> findAll() {
        return (List<LanguageModel>) langRepository.findAll();
    }

    @Override
    public LanguageModel findById(Integer id) {
        return langRepository.getOne(id);
    }

    @Override
    public LanguageModel save(LanguageModel lang) {
        return langRepository.save(lang);
    }

    @Override
    public void deleteById(Integer id) {
        langRepository.deleteById(id);
    }

    @Override
    public LanguageModel findByCode(String code) {
        return langRepository.findByCode(code);
    }

    @Override
    public boolean existsById(Integer id) {
        return langRepository.existsById(id);
    }
}
