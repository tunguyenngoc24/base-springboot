package balance.service.languages;

import balance.model.Language.LanguageModel;

import java.util.List;
import java.util.Optional;

public interface ILanguagesService {
    List<LanguageModel> findAll();
    LanguageModel findById(Integer id);
    LanguageModel save(LanguageModel lang);
    void deleteById(Integer id);
    LanguageModel findByCode(String code);
    boolean existsById(Integer id);
}
