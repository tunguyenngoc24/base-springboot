package balance.service.category;

import balance.model.category.Category;

import java.util.List;
import java.util.Optional;

public interface ICategoryService {
    List<Category> findAll();
    Category findById(Integer id);
    Optional<Category> getOne(Integer id);
    Category save(Category category);
    void deleteById(Integer id);
    boolean existsById(Integer id);
}
