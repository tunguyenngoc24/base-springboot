package balance.service.category;

import balance.model.category.Category;
import balance.model.role.Role;
import balance.model.role.UserRole;
import balance.repository.category.ICategoryRepository;
import balance.repository.role.IRoleRepository;
import balance.repository.role.IUserRoleRepository;
import balance.service.role.IRoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CategoryService implements ICategoryService {
    @Autowired
    private ICategoryRepository categoryRepository;
    @Override
    public List<Category> findAll() {
        return categoryRepository.findAll();
    }

    @Override
    public Category findById(Integer id) {
        return categoryRepository.getOne(id);
    }

    @Override
    public Optional<Category> getOne(Integer id) {
        return categoryRepository.findById(id);
    }

    @Override
    public Category save(Category category) {
        return categoryRepository.save(category);
    }

    @Override
    public void deleteById(Integer id) {
        categoryRepository.deleteById(id);
    }

    @Override
    public boolean existsById(Integer id) {
        return categoryRepository.existsById(id);
    }
}
