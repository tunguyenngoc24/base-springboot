package balance.service.role;

import balance.model.role.Role;
import balance.model.role.UserRole;
import balance.repository.role.IRoleRepository;
import balance.repository.role.IUserRoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class RoleService implements IRoleService {
    @Autowired
    private IRoleRepository roleRepository;
    @Autowired
    private IUserRoleRepository userRoleRepository;
    @Override
    public List<Role> findAll() {
        return roleRepository.findAll();
    }

    @Override
    public List<Role> findAllByUserId(Integer userId) {
        ArrayList<Role> roleList = new ArrayList<>();
        List<UserRole> userRoleList = userRoleRepository.findAllByUserId(userId);
        if (userRoleList.isEmpty()) {
            return null;
        } else {
            for (UserRole userRole : userRoleList) {
                roleList.add(userRole.getRole());
            }
        }
        return roleList;
    }

    @Override
    public Role getOne(Integer id) {
        return roleRepository.getOne(id);
    }

    @Override
    public Role save(Role role) {
        return roleRepository.save(role);
    }

    @Override
    public void deleteById(Integer id) {
        roleRepository.deleteById(id);
    }

    @Override
    public boolean existsById(Integer id) {
        return roleRepository.existsById(id);
    }
}
