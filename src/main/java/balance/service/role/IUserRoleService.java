package balance.service.role;

import balance.model.role.Role;
import balance.model.role.UserRole;

import java.util.List;

public interface IUserRoleService {
    UserRole save(UserRole userRole);
    void delete(UserRole userRole);
}
