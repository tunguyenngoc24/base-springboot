package balance.service.role;

import balance.model.role.Role;
import balance.model.role.UserRole;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public interface IRoleService {
    List<Role> findAll();
    List<Role> findAllByUserId(Integer userId);
    Role getOne(Integer id);
    Role save(Role role);
    void deleteById(Integer id);
    boolean existsById(Integer id);
}
