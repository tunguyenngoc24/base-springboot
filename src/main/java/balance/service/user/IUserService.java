package balance.service.user;

import balance.model.user.User;

import java.util.List;
import java.util.Optional;

public interface IUserService {
    List<User> findAll();
    List<User> findAllPaging(Integer pageNo, Integer pageSize);
    Optional<User> findById(Integer id);
    User getOne(Integer id);
    User save(User user);
    void deleteById(Integer id);
    User findByEmail(String email);
    boolean existsById(Integer id);
}
