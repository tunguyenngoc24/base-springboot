package balance.service.Menu;

import balance.model.Menu.Menus;

import java.util.List;

public interface IMenuService {
    List<Menus> findAll();
    Menus save(Menus menus);
    void deleteById(Integer id);
    boolean existsById(Integer id);
}
