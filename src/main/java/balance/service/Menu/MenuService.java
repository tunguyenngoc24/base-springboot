package balance.service.Menu;

import balance.model.Menu.Menus;
import balance.repository.Menu.IMenuRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class MenuService implements IMenuService {
    @Autowired
    private IMenuRepository menuRepository;

    @Override
    public List<Menus> findAll() {
        return (List<Menus>) menuRepository.findAll();
    }

    @Override
    public Menus save(Menus menus) {
        return menuRepository.save(menus);
    }

    @Override
    public void deleteById(Integer id) {
        menuRepository.deleteById(id);
    }

    @Override
    public boolean existsById(Integer id) {
        return menuRepository.existsById(id);
    }
}
