package balance.model.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name = "user_elements")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class UserElement {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private Integer userId;
    //@ManyToOne
    //@JoinColumn
    //private User user;
    @JsonProperty
    private String name;
    @JsonProperty
    private String value;
    @JsonProperty
    private Integer type;
}
