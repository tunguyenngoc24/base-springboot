package balance.model.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name = "user_translators")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class UserTranslator {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private Integer userId;
    //@ManyToOne
    //@JoinColumn
    //private User user;
    private Integer languageId;
    //@ManyToOne
    //@JoinColumn
    //private Language language;
    @JsonProperty
    private String reasonForRegistration;
}
