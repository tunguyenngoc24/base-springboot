package balance.model.user;

import balance.model.BaseModel;
import balance.model.role.UserRole;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Time;
import java.util.List;

@Entity
@Table(name = "users")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class User extends BaseModel {
    @JsonProperty
    @Length(max = 256)
    private String name;
    @JsonProperty
    @Length(max = 256)
    private String nickName;
    private String avatar;
    @JsonProperty
    @NotNull
    @Length(max = 256)
    private String email;
    @NotNull
    @Length(max = 256)
    private String password;
    @JsonProperty
    private Time dob;
    @JsonProperty
    @Length(max = 20)
    private String phone;
    @JsonProperty
    private String address;
    @Length(max = 256)
    private String bankAccount;
    private Boolean termsOption;
    private Boolean provideInformation;
    private Boolean termsAgree;
    private Boolean receiveNotification;
    private Boolean emailConfirm;
    private Boolean isMember;
    @Length(max = 20)
    private String levelInCenter;
    @Length(max = 20)
    private String levelInAcademy;
    private Boolean status;
    private Boolean online;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "user")
    private List<UserRole> userRoleList;

    //@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    //private UserTranslator userTranslators;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setDob(Time dob) {
        this.dob = dob;
    }

    public Time getDob() {
        return dob;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setTermsOption(Boolean termsOption) {
        this.termsOption = termsOption;
    }

    public Boolean getTermsOption() {
        return termsOption;
    }

    public void setProvideInformation(Boolean provideInformation) {
        this.provideInformation = provideInformation;
    }

    public Boolean getProvideInformation() {
        return provideInformation;
    }

    public void setTermsAgree(Boolean termsAgree) {
        this.termsAgree = termsAgree;
    }

    public Boolean getTermsAgree() {
        return termsAgree;
    }

    public void setReceiveNotification(Boolean receiveNotification) {
        this.receiveNotification = receiveNotification;
    }

    public Boolean getReceiveNotification() {
        return receiveNotification;
    }

    public void setEmailConfirm(Boolean emailConfirm) {
        this.emailConfirm = emailConfirm;
    }

    public Boolean getEmailConfirm() {
        return emailConfirm;
    }

    public void setIsMember(Boolean isMember) {
        this.isMember = isMember;
    }

    public Boolean getIsMember() {
        return isMember;
    }

    public void setLevelInCenter(String levelInCenter) {
        this.levelInCenter = levelInCenter;
    }

    public String getLevelInCenter() {
        return levelInCenter;
    }

    public void setLevelInAcademy(String levelInAcademy) {
        this.levelInAcademy = levelInAcademy;
    }

    public String getLevelInAcademy() {
        return levelInAcademy;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setOnline(Boolean online) {
        this.online = online;
    }

    public Boolean getOnline() {
        return online;
    }
}
