package balance.model.user;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.sql.Time;

public class UserViewModel {
    @JsonProperty
    private String name;
    @JsonProperty
    private String nickName;
    @JsonProperty
    private String email;
    @JsonProperty
    private Time dob;
    @JsonProperty
    private String phone;
    @JsonProperty
    private String address;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setDob(Time dob) {
        this.dob = dob;
    }

    public Time getDob() {
        return dob;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setAddress(String email) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }
}
