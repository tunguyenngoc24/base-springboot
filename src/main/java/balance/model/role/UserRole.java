package balance.model.role;

import balance.model.BaseModel;
import balance.model.user.User;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name = "user_roles")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class UserRole extends BaseModel {
    //private Integer userId;
    //private Integer roleId;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "role_id", nullable = false)
    @JsonProperty("role_id")
    private Role role;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    @JsonProperty("user_id")
    private User user;

    public void setRole(Role role) {
        this.role = role;
    }

    public Role getRole () {
        return role;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser () {
        return user;
    }
}
