package balance.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(
        value = {"Ctime","Mtime"},
        allowGetters = true
)
public abstract class BaseModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "Ctime", nullable = false, updatable = false)
    @CreatedDate
    private Date ctime;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "Mtime")
    @LastModifiedDate
    private Date mtime;

    public Integer getId() {return id;}

    public Date getCtime() { return ctime; }
    public void setCtime(Date ctime) { this.ctime = ctime;}

    public Date getMtime() { return mtime; }
    public void setMtime(Date mtime) { this.mtime = mtime;}
}
