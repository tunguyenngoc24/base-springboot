package balance.model.Language;

import balance.model.BaseModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "languages")
public class LanguageModel extends BaseModel {
    @NotNull
    @Size(max = 5)
    private String code;

    private String name;

    public String getCode() {return code;}
    public void setCode(String code) {
        this.code = code;
    }

    public  String getName() {return name;}
    public void setName(String name) {
        this.name = name;
    }
}
