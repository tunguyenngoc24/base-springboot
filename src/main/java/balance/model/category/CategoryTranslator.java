package balance.model.category;

import balance.model.BaseModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "category_translators")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class CategoryTranslator extends BaseModel {
    private Integer categoryId;
    private Integer languageId;
    private String name;
    private String level;
    private String description;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getLevel() {
        return level;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
