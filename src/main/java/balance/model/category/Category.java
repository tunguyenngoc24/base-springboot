package balance.model.category;

import balance.model.BaseModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "categories")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Category extends BaseModel {
    private Float price;
    private Integer time;
    private Integer parentId;
    private Integer displayOrder;
    private Integer center;
    private Boolean isProduct;
    private Boolean isAccompaniedService;

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getPrice() {
        return price;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public Integer getTime() {
        return time;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setDisplayOrder(Integer displayOrder) {
        this.displayOrder = displayOrder;
    }

    public Integer getDisplayOrder() {
        return displayOrder;
    }

    public void setCenter(Integer center) {
        this.center = center;
    }

    public Integer getCenter() {
        return center;
    }

    public void setIsProduct(Boolean isProduct) {
        this.isProduct = isProduct;
    }

    public Boolean getIsProduct() {
        return isProduct;
    }

    public void setIsAccompaniedService(Boolean isAccompaniedService) {
        this.isAccompaniedService = isAccompaniedService;
    }

    public Boolean getIsAccompaniedService() {
        return isAccompaniedService;
    }
}
