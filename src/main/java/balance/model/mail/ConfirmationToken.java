package balance.model.mail;

import balance.model.user.User;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "confirmation_tokens")
public class ConfirmationToken {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String token;

    @Temporal(TemporalType.TIMESTAMP)
    private Date expirate;

    @Temporal(TemporalType.TIMESTAMP)
    private Date cTime;

    private Integer userId;
    @OneToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "id")
    private User user;

    // getters and setters
    public String getConfirmationToken() {
        return token;
    }

    public void setConfirmationToken(String token) {
        this.token = token;
    }

    public Date getcTime() {
        return cTime;
    }

    public void setcTime(Date cTime) {
        this.cTime = cTime;
    }

    public Date getExpirate() {
        return expirate;
    }

    public void setExpirate(Date expirate) {
        this.expirate = expirate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
