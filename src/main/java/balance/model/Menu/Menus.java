package balance.model.Menu;

import balance.model.BaseModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "menus")
public class Menus extends BaseModel {

    @Column(name = "parent_id")
    private Integer parentId;

    @Column(name = "display_order")
    private Integer displayOrder;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "menus")
    private MenuTranslators menuTranslators;

    // Hibernate requires a no-arg constructor
    public Menus() {}

    public Menus(Integer parentId, Integer displayOrder) {
        this.parentId = parentId;
        this.displayOrder = displayOrder;
    }

    public Integer getParentId() {return parentId;}
    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public  Integer getDisplayOrder() {return displayOrder;}
    public void setDisplayOrder(Integer displayOrder) {
        this.displayOrder = displayOrder;
    }

    public MenuTranslators getMenuTranslators() {
        return menuTranslators;
    }
    public void setMenuTranslators(MenuTranslators menuTranslators) {
        this.menuTranslators = menuTranslators;
    }



}
