package balance.model.Menu;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class MenuInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotNull
    @Column(name = "language_code")
    private String code;

    @NotNull
    @Column(name = "menu_id")
    private Integer menuId;

    private String name;
    private String link;


    public MenuInfo() {

    }

    public MenuInfo(String code, String name, String link, Integer menuId) {
        this.code = code;
        this.name = name;
        this.link = link;
        this.menuId = menuId;
    }

    public Integer getId () {return id;}
    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMenuId () {return menuId;}
    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    public String getCode () {return code;}
    public void setCode(String code) {
        this.code = code;
    }

    public String getName () {return name;}
    public void setName(String name) {
        this.name = name;
    }

    public  String getLink() {return link;}
    public void setLink(String link) {
        this.link = link;
    }

}
