package balance.model.Menu;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "menu_translators")
public class MenuTranslators {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotNull
    @Column(name = "language_code")
    private String code;

    private String name;
    private String link;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "menu_id", nullable = false)
    @JsonIgnore
    private Menus menus;

    public MenuTranslators() {

    }

    public MenuTranslators(String code, String name, String link) {
        this.code = code;
        this.name = name;
        this.link = link;
    }


    public Integer getId () {return id;}
    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode () {return code;}
    public void setCode(String code) {
        this.code = code;
    }

    public String getName () {return name;}
    public void setName(String name) {
        this.name = name;
    }

    public  String getLink() {return link;}
    public void setLink(String link) {
        this.link = link;
    }

    @JsonIgnore
    public Menus getMenus() {return menus;}

    @JsonProperty
    public void setMenus(Menus menus) {
        this.menus = menus;
    }
}
