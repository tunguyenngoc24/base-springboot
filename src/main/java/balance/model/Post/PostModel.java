package balance.model.Post;

import balance.model.BaseModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.sql.Time;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "post")
public class PostModel extends BaseModel {

    private Integer parentId;
    private Integer displayOrder;
    private Integer type;
    private Time postTime;
    private Boolean status;

    public Integer getParentId() {return parentId;}
    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getDisplayOrder() {return displayOrder;}
    public void setDisplayOrder(Integer displayOrder) {
        this.displayOrder = displayOrder;
    }

    public Integer getType() {return type;}
    public void setType(Integer type) {
        this.type = type;
    }

    public Time getPostTime() {return postTime;}
    public void setPostTime(Time postTime) {
        this.postTime = postTime;
    }

    public Boolean getStatus() {return status;}
    public void setStatus(Boolean status) {
        this.status = status;
    }

}
